<?php

namespace Drupal\super_admin_dashboard\Form;

use Drupal\node\Entity\NodeType;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class SuperAdminFilterOptions extends FormBase {

  public function buildForm(array $form, FormStateInterface $form_state) {
    $type_options = [];
    $field_options = [];
    $entity_type_id = 'node';
    $node_types = NodeType::loadMultiple();
    // Super Admin Services to server data.
    $super_admin_service = \Drupal::service('super_admin_dashboard.table_content');
    foreach ($node_types as $node_type) {
      $type_options[$node_type->get('type')] = $node_type->get('name');
      $content_type_data = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $node_type->id());
      foreach ($content_type_data as $field_name => $field_definition) {
        if (!in_array($field_definition->getType(), $field_options)) {
          if (!empty($field_definition->getTargetBundle()) && $field_definition->getType() != 'comment') {
            $field_options[$field_definition->getName()] = $field_definition->getLabel();
            $admin_field_info[$field_definition->getName()] = $field_definition->getType();
          }
        }
      }
    }
    // Building table content.
    $table_content = $super_admin_service->saTableContent($form_state->getValue('columns_settings'), $admin_field_info, $form_state->getValue('type_filter'), $form_state->getValue('key_search'));
    $form['key_search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Key Search'),
      '#ajax' => [
        'wrapper' => 'sa-col-fieldset-wrapper',
        'callback' => '::promptCallback',
        'event' => 'change',
      ],
      '#attributes' => [
        'data-disable-refocus' => 'true',
      ],
    ];
    $form['type_filter'] = [
      '#type' => 'select',
      '#title' => $this->t('Content Type'),
      '#multiple' => TRUE,
      '#options' => $type_options,
      '#description' => 'Filter by content type_filter.',
      '#ajax' => [
        'wrapper' => 'sa-col-fieldset-wrapper',
        'callback' => '::promptCallback',
      ],
    ];
    $form['columns_settings'] = [
      '#type' => 'select',
      '#title' => $this->t('Columns'),
      '#multiple' => TRUE,
      '#options' => $field_options,
      '#description' => '"<b>---</b>" indicates empty field & "<b>***</b>" indicates field dosenot exists.',
      '#ajax' => [
        'wrapper' => 'sa-col-fieldset-wrapper',
        'callback' => '::promptCallback',
      ],
    ];
    $form['sa_col_fieldset'] = [
      '#type' => 'container',
      '#open' => TRUE,
      '#attributes' => ['id' => 'sa-col-fieldset-wrapper'],
    ];
    $header = [
      'nid' => $this->t('Node ID'),
      'title' => $this->t('Title'),
    ];
    if (!empty($form_state->getValue('columns_settings'))) {
      $header = $super_admin_service->saTableHeader($form_state->getValue('columns_settings'), $header);
    }
    $form['sa_col_fieldset']['table'] = [
      '#type' => 'tableselect',
      '#title' => $this->t('Users'),
      '#header' => $header,
      '#options' => $table_content,
      '#empty' => $this->t('No data found'),
    ];
    $form['#theme'] = 'super_admin_dashboard';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'super_admin_dashboard_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = $this->messenger();
    $sa_col_ans = $form_state->getValue('columns_settings');
    $messenger->addMessage($this->t('Super admin module submit: @sa_col_ans', ['@answer' => $sa_col_ans]));
    return;
  }

  /**
   * {@inheritdoc}
   */
  public function promptCallback(array &$form, FormStateInterface $form_state) {
    return $form['sa_col_fieldset'];
  }

}
