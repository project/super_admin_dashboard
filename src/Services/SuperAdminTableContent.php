<?php

namespace Drupal\super_admin_dashboard\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;

/**
 * Class AggregatedContentBulkUpdate.
 */
class SuperAdminTableContent {

  /**
   * Entity type manager is used to fetch tags list.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct the KapostTagging.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager is used to fetch tags list.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Returns tags of requested Vocabulary.
   */
  public function saTableHeader($columns_settings, $header) {
    foreach ($columns_settings as $selected_option => $selected_option_vaule) {
      $header[$selected_option_vaule] = $selected_option_vaule;
    }
    return $header;
  }

  public function saTableContent($columns_settings, $admin_field_info, $type_filters, $key_search) {
    $data = [];
    $type_filter_data = [];
    $i = 0;
    $query = \Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->sort('changed', 'DESC')
      ->range(0, 50);
    if (!empty($type_filters)) {
      foreach ($type_filters as $type_filter_id => $type_filter_val) {
        $type_filter_data[] = $type_filter_val;
      }
      $query->condition('type', $type_filter_data, 'IN');
    }
    $nids = $query->execute();
    $nodes = Node::loadMultiple($nids);
    foreach ($nodes as $nid => $node) {
      //Check for search box keyword in Title field value.
      if (!empty($key_search)) {
        if (stripos($node->label(), $key_search) !== false) {
          $data = $this->dataBuilder($data, $i, $node, $columns_settings, $admin_field_info);
        }
      }
      else {
        $data = $this->dataBuilder($data, $i, $node, $columns_settings, $admin_field_info);
      }
      $i++;
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function dataBuilder($data, $i, $node, $columns_settings, $admin_field_info) {
    $data[$i] = ['nid' => $node->id(), 'title' => $node->label()];
    if (!empty($columns_settings)) {
      foreach ($columns_settings as $selected_option_col => $selected_option_vaule_col) {
        if ($node->hasField($selected_option_vaule_col)) {
          $multi_field_data = '';
          $multi_field_data = $this->getSaFieldData($node, $admin_field_info[$selected_option_vaule_col], $selected_option_vaule_col, $i);
          if ($multi_field_data) {
            $data[$i][$selected_option_vaule_col] = $multi_field_data;
          }
          else {
            $data[$i][$selected_option_vaule_col] = '---';
          }
        }
        else {
          $data[$i][$selected_option_vaule_col] = '***';
        }
      }
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getSaFieldData($node, $admin_field_info, $selected_option_vaule_col, $i) {
    $multi_field_data = '';
    switch ($admin_field_info) {
      case 'image':
        if ($node->$selected_option_vaule_col->isEmpty()) {
          $multi_field_data = '---';
        }
        else {
          $multi_field_data = \Drupal::service('file_url_generator')->generateAbsoluteString($node->$selected_option_vaule_col->entity->getFileUri());
        }
        break;
      case 'list_string':
        if (count($node->get($selected_option_vaule_col)->getValue()) > 1) {
          foreach ($node->get($selected_option_vaule_col)->getValue() as $multi_field_val) {
            $multi_field_data = $multi_field_data . $multi_field_val['value'] . '||';
          }
        }
        else {
          $multi_field_data = $node->get($selected_option_vaule_col)->getValue()[0]['value'];
        }
        break;
      case 'entity_reference':
        if (count($node->get($selected_option_vaule_col)->getValue()) > 1) {
          foreach ($node->get($selected_option_vaule_col)->getValue() as $key => $node_field_data) {
            $name = Term::load($node_field_data['target_id'])->getName();
            $multi_field_data = $multi_field_data . $name . '||';
          }
        }
        else {
          $multi_field_data = Term::load($node->get($selected_option_vaule_col)->getValue()[0]['target_id'])->getName();
        }
        break;
      case 'text_with_summary':
        $multi_field_data = strip_tags($node->get($selected_option_vaule_col)->getValue()[0]['value']);
        break;
      case 'boolean':
        if ($node->get($selected_option_vaule_col)->getValue()[0]['value'] == 1) {
          $multi_field_data = 'Yes';
        }
        else {
          $multi_field_data = 'No';
        }
        break;
      default:
        $multi_field_data = $node->get($selected_option_vaule_col)->getValue()[0]['value'];
        break;
    }
    return $multi_field_data;
  }

}
