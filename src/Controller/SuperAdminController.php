<?php

namespace Drupal\super_admin_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller routines for super admin module.
 */
class SuperAdminController extends ControllerBase {

  /**
   * Super Admin form.
   */
  public function superadmindata() {
    $form_class = '\Drupal\super_admin_dashboard\Form\SuperAdminFilterOptions';
    $build['form'] = \Drupal::formBuilder()->getForm($form_class);
    return $build;
  }

}
