# Super Admin Dashboard

- **Project site**: https://www.drupal.org/project/super_admin_dashboard
- **Code**: https://www.drupal.org/project/super_admin_dashboard/git-instructions
- **Issues**: https://www.drupal.org/project/issues/super_admin_dashboard

## INTRODUCTION

Suer Admin Dashboard is an easy interface access content with column add remove 
functionality.

## COLUMN SELECT:

It allows user to choose columns to be displayed and hidden.

## AVAILABLE FILTERS

Content type display:
User will be able to select content types that are required to be 
displayed in the dashboard.

Keyword search:
User will be able to search with text contained in Title field.

A default limit of 50 items is set as maximum limit.

Form works on AJAX auto submit functionality.

## REQUIREMENTS

No special requirements for this module.


## INSTALLATION

* Install as usual, see http://drupal.org/node/70151 for further information.


## CONFIGURATION

Dashboard can be accessed at admin/super_admin_dashboard
